var app = new PIXI.Application( window.innerWidth, window.innerHeight, {backgroundColor : 0x1099bb});
document.body.appendChild(app.view);

var container = new PIXI.Container();

app.stage.addChild(container);

var texture = PIXI.Texture.fromImage('img/caccarosa.png');

// Create a 5x5 grid of bunnies
for (var i = 0; i < 25; i++) {
    var bunny = new PIXI.Sprite(texture);
    bunny.anchor.set(0.5);
    bunny.x = (i % 5) * 80;
    bunny.y = Math.floor(i / 5) * 80;
    container.addChild(bunny);

    bunny.scale.x = .1;
    bunny.scale.y = .1;

}

// Center on the screen
container.x = app.screen.width / 2;
container.y = app.screen.height / 2;

// fisso un punto al centro del container
container.pivot.x = container.width / 2;
container.pivot.y = container.height / 2;

// Listen for animate update
app.ticker.add(function(delta) {
    // rotate the container!
    // use delta to create frame-independent transform
    container.rotation += 0.01 * delta;
});


// add class CANVAS
var zizzi = document.getElementsByTagName("CANVAS")[0];   // Get the first <zizzi> element in the document
var att = document.createAttribute("id");       // Create a "class" attribute
att.value = "culo";                           // Set the value of the class attribute
zizzi.setAttributeNode(att);
